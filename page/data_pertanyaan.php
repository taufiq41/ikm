<?php 
	
	if(isset($_POST['tambah'])){

		$penilaian_id = $_POST['penilaian_id'];
		$pertanyaan = $_POST['pertanyaan'];

		$query = "INSERT INTO pertanyaan(penilaian_id, pertanyaan) VALUES('$penilaian_id','$pertanyaan')";

		$result = mysqli_query($koneksi, $query);
		if($result){
			?>
			<script>alert('Tambah data berhasil');</script>
			<?php
		}else{
			?>
			<script>alert('Tambah data gagal');</script>
			<?php
		}
	}
?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="?">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Master Pertanyaan</a></li>
  </ol>
</nav>
<div class="card">
  <div class="card-body">
    <h5 class="card-title">Tambah Master Pertanyaan</h5>
    <h6 class="card-subtitle mb-2 text-muted">Isi semua data dibawah dan tidak boleh kosong</h6>

    <form action="?p=data_pertanyaan" method="POST" enctype="multipart/form-data">
	  <div class="form-group">
	    <label for="exampleFormControlInput1">Penilaian</label>
	    <select name="penilaian_id" id="" class="form-control">
	    	<?php 

	    		$query = "SELECT * FROM penilaian";

	    		$result = mysqli_query($koneksi, $query);

	    		while($row = mysqli_fetch_assoc($result)){
	    			?>
	    			<option value="<?php echo $row['id']; ?>"><?php echo $row['objek_penilaian']; ?></option>
	    			<?php
	    		}
	    	?>
	    </select>
	  </div>
	  <div class="form-group">
	    <label for="inoutObjekPenilaian">Pertanyaan</label>
	    <input type="text" name="pertanyaan" class="form-control" placeholder="Masukan Pertanyaan" required>
	  </div>
	  <button type="submit" name="tambah" class="btn btn-primary">Tambah</button>
	</form>
  </div>
</div>
<br>
<div class="card">
	<div class="card-body">
		<table class="table">
  			<thead class="thead-dark">
  				<tr>
  					<th>#</th>
  					<th>Target</th>
  					<th>Pertanyaan</th>
  					<th>Perintah</th>
  				</tr>
  				<?php 

  					$query = "SELECT pn.target_penilaian, pn.objek_penilaian, p.* FROM pertanyaan p INNER JOIN penilaian pn ON p.penilaian_id=pn.id";

    				$result = mysqli_query($koneksi, $query);
    				$no = 1;

    				while($row = mysqli_fetch_assoc($result)){
    					?>
    					<tr>
		  					<td><?php echo $no++; ?></td>
		  					<td><?php echo $row['target_penilaian']; ?></td>
		  					<td><?php echo $row['pertanyaan']; ?></td>
		  					
		  					<td><a href="">Hapus</a>||<a href="">Edit</a></td>
		  				</tr>
    					<?php
    				}
  				?>
  			</thead>
  		</table>
	</div>
</div>
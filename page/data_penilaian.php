<?php 
	
	if(isset($_POST['tambah'])){

		$target_penilaian = $_POST['target_penilaian'];
		$objek_penilaian = $_POST['objek_penilaian'];
		$logo_objek_penilaian = '';

		$namaFile = time().$_FILES['logo_objek_penilaian']['name'];
		$namaSementara = $_FILES['logo_objek_penilaian']['tmp_name'];

		// tentukan lokasi file akan dipindahkan
		$dirUpload = "uploads/";

		// pindahkan file
		$terupload = move_uploaded_file($namaSementara, $dirUpload.$namaFile);

		if ($terupload) {
		 	
			$query = "INSERT INTO penilaian(target_penilaian, objek_penilaian, logo_objek_penilaian) VALUES('$target_penilaian','$objek_penilaian','$namaFile')";

			$result = mysqli_query($koneksi, $query);
			if($result){
				?>
				<script>alert('Tambah data berhasil');</script>
				<?php
			}else{
				?>
				<script>alert('Tambah data gagal');</script>
				<?php
			}

		} else {
		    ?>
			<script>alert('Tambah data gagal');</script>
			<?php
		}
	}
?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="?">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Master Penilaian</a></li>
  </ol>
</nav>
<div class="card">
  <div class="card-body">
    <h5 class="card-title">Tambah Master Penilaian</h5>
    <h6 class="card-subtitle mb-2 text-muted">Isi semua data dibawah dan tidak boleh kosong</h6>

    <form action="?p=data_penilaian" method="POST" enctype="multipart/form-data">
	  <div class="form-group">
	    <label for="exampleFormControlInput1">Target Penilaian</label>
	    <input type="text" name="target_penilaian" class="form-control" placeholder="Masukan Target Penilaian" required>
	  </div>
	  <div class="form-group">
	    <label for="inoutObjekPenilaian">Objek Penilaian</label>
	    <input type="text" name="objek_penilaian" class="form-control" placeholder="Masukan Objek Penilaian" required>
	  </div>
	  <div class="form-group">
	    <label for="exampleFormControlFile1">Logo (.png)</label>
	    <input type="file" class="form-control-file" name="logo_objek_penilaian">
	  </div>
	  <button type="submit" name="tambah" class="btn btn-primary">Tambah</button>
	</form>
  </div>
</div>
<br>
<div class="card">
	<div class="card-body">
		<table class="table">
  			<thead class="thead-dark">
  				<tr>
  					<th>#</th>
  					<th>Target</th>
  					<th>Objek</th>
  					<th>Logo</th>
  					<th>Perintah</th>
  				</tr>
  				<?php 

  					$query = "SELECT * FROM penilaian";

    				$result = mysqli_query($koneksi, $query);
    				$no = 1;
    				while($row = mysqli_fetch_assoc($result)){
    					?>
    					<tr>
		  					<td><?php echo $no++; ?></td>
		  					<td><?php echo $row['target_penilaian']; ?></td>
		  					<td><?php echo $row['objek_penilaian']; ?></td>
		  					<td><img src="<?php echo 'uploads/'.$row['logo_objek_penilaian']; ?>" alt="" width="200px" height="200px"></td>
		  					<td><a href="">Hapus</a>|||<a href="">Edit</a></td>
		  				</tr>
    					<?php
    				}
  				?>
  			</thead>
  		</table>
	</div>
</div>
<?php 
	session_start();
	if(!isset($_SESSION['username'])){
		$host  = $_SERVER['HTTP_HOST'];
		$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
		$extra = 'page/login.php';
		header("Location: http://$host$uri/$extra");
	}
	include('config/koneksi.php');
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>IKM KU</title>
	<link rel="stylesheet" href="assets/bootstrap-4.3.1-dist/css/bootstrap.min.css">
	<script src="assets/jquery-3.3.1.slim.min.js"></script>
	<script src="assets/bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
	<script src="assets/popper.min.js"></script>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <a class="navbar-brand" href="#">Logo2</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarSupportedContent">
	    <ul class="navbar-nav mr-auto">
	      <li class="nav-item active">
	        <a class="nav-link" href="">Home <span class="sr-only">(current)</span></a>
	      </li>
	      <li class="nav-item active dropdown">
	        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	          Data
	        </a>
	        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
	          <a class="dropdown-item" href="<?php echo '?p=data_penilaian'; ?>">Master Penilaian</a>
	          <a class="dropdown-item" href="<?php echo '?p=data_pertanyaan'; ?>">Master Pertanyaan</a>
	          <a class="dropdown-item" href="<?php echo '?p=data_pertanyaan'; ?>">Master Jawaban</a>
	          <div class="dropdown-divider"></div>
	          <a class="dropdown-item" href="<?php echo '?p=report'; ?>">Report</a>
	        </div>
	      </li>
	    </ul>
	    <ul class="navbar-nav my-2 my-lg-0">
	    	<li class="nav-item active"> <a href="#" class="nav-link" >Logout</a></li>
	    </ul>
	    <!-- <form class="form-inline my-2 my-lg-0">
	      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
	      <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Search</button>
	    </form> -->
	  </div>
	</nav>
	<br>
	<div class="container-fluid">

		<?php 
			
			if(isset($_GET['p'])){
				$page = $_GET['p'];

				if($page == 'data_penilaian'){
					include('page/data_penilaian.php');
				}else if($page == 'data_pertanyaan'){
					include('page/data_pertanyaan.php');
				}else if($page == 'data_jawaban'){
					include('page/data_jawaban.php');
				}else if($page == 'report'){
					include('page/report.php');
				}else{
					include('page/home.php');
				}
			}else{
				include('page/home.php');
			}
		?>
	</div>
</body>
</html>
<?php 
	
	include('../config/koneksi.php');
	$content = trim(file_get_contents("php://input"));
	$decode = json_decode($content, true);

	$pertanyaan_id = $decode['pertanyaan_id'];

	$query = "SELECT 
		SUM(CASE WHEN nilai = 5 THEN 1 ELSE 0 END) as lima,
	    SUM(CASE WHEN nilai = 4 THEN 1 ELSE 0 END) as empat,
	    SUM(CASE WHEN nilai = 3 THEN 1 ELSE 0 END) as tiga,
	    SUM(CASE WHEN nilai = 2 THEN 1 ELSE 0 END) as dua,
	    SUM(CASE WHEN nilai = 1 THEN 1 ELSE 0 END) as satu,
	    SUM(CASE WHEN nilai = 0 THEN 1 ELSE 0 END) as nol
	    
	FROM `jawaban` WHERE pertanyaan_id=$pertanyaan_id";

	$result = mysqli_query($koneksi, $query);
	

	$data = mysqli_fetch_assoc($result);	
	
	
	echo json_encode($data);

?>